# CAN TRADING COMP


# Setting up

## Dependencies

```
lxml
selenium
```

## Binance Chain stuff
https://docs.binance.org/api-reference/dex-api/paths.html#apiv1transactions

## GSheet stuff
For all info:
https://developers.google.com/sheets/api/quickstart/python?authuser=2

```
pip install --upgrade google-api-python-client google-auth-httplib2 google-auth-oauthlib
```

# How it works

A [selenium](https://selenium-python.readthedocs.io/installation.html) scraper pulls all of the CAN token holders off this page:
https://explorer.binance.org/asset/holders/CAN-677

It then adds them to an array of addresses.

Then it queries each address for all trades since 01 July 2019, using this API:
> "https://dex.binance.org/api/v1/trades?symbol=CAN-677_BNB&start=1561939200000&limit=1000&total=1&address="


Then it adds them all together and writes them to this spreadsheet:
https://docs.google.com/spreadsheets/d/1zaA2BWsqUQfdIj_Uk2RgUD1OZVMtRFllqyeGR0VJFRY

# HOW TO RUN

```
python leaderboard.py
```

Simples!

If you would like to enhance it, please submit a pull request.

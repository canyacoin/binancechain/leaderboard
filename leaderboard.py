from __future__ import print_function
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request

# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/spreadsheets']

# The ID and range of a sample spreadsheet.
SAMPLE_SPREADSHEET_ID = '1zaA2BWsqUQfdIj_Uk2RgUD1OZVMtRFllqyeGR0VJFRY'
SAMPLE_RANGE_NAME = 'Sheet1!A4:E4'
value_input_option = "USER_ENTERED"

import json
import requests
from lxml import html
import time
from selenium import webdriver
from datetime import datetime

driver = webdriver.Chrome()
#Maximize window so that pagination is visible
driver.maximize_window()
#TO DO: Update pages QTY with class query function
#FOR NOW: manually adjust
pages = 16

arrayAddresses =[]
arrayQty = []
arrayTrades = []
arrayLinks = []

#Ping URL
def ping(url):
    response = requests.get(url)
    if response.status_code == 200:
        return json.loads(response.content.decode('utf-8'))
    else:
        return None

# Scraper: page NEXT
def next():
    # Get button you are going to click by its id ( also you could us find_element_by_css_selector to get element by css selector)
    button_element = driver.find_element_by_class_name('bnc-pagination-next')
    button_element.click()

# Scraper: page scrape
def addrOutput(html):
    addresses = html.xpath('//td[@class=""]/text()')
    tags = html.xpath('.//tr[@class="bnc-table-row bnc-table-row-level-0"]')
    total_tags = []
    for tag in tags:
        total_tags.append(tag.text_content())

    for y in range(0, len(total_tags)):
        str = total_tags[y]
        bnbI = str.find("bnb", 0, 10)
        commaI = str.find(addresses[y], 0, 80)
        addr = str[bnbI:commaI]
        arrayAddresses.append(addr)
        print('added: ' + addr)

def main():

    #Start by building the CAN holders array
    url = 'https://explorer.binance.org/asset/holders/CAN-677'
    driver.get(url)
    page1 = requests.get(url)
    tree = html.fromstring(page1.content)

    for x in range(0, pages-1):
        next()
        page2 = driver.execute_script("return document.documentElement.outerHTML;")
        tree = html.fromstring(page2)
        print('page: {}'.format(x+1))
        addrOutput(tree)
        time.sleep(3)

    # Now set up GSheets

    """Shows basic usage of the Sheets API.
    Prints values from a sample spreadsheet.
    """
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server()
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    service = build('sheets', 'v4', credentials=creds)

    # Write Last Updated Variable
    values = [
        [
        datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        ],
        ]
    data = [
        {
        'range': 'Sheet1!B2',
        'values': values
        },
    ]
    body = {
        'valueInputOption': value_input_option,
        'data': data
    }
    result = service.spreadsheets().values().batchUpdate(
        spreadsheetId=SAMPLE_SPREADSHEET_ID, body=body).execute()
    print('{0} cells updated.'.format(result.get('totalUpdatedCells')))


    #Now query for trade totals
    start_time = 1561903200000 #2019/07/01, 0:00 AM (AEST)
    end_time = 1564581599000 #2019/07/31, 11:59 PM (AEST)
    current_time = int(round(time.time() * 1000))
    tx_url = "https://dex.binance.org/api/v1/trades?symbol=CAN-677_BNB&start=" + str(start_time) + "&end=" + str(end_time) + "&limit=1000&total=1&address="""
    traders = 0

    for x in range(0, len(arrayAddresses)):
        tx_addr = arrayAddresses[x]
        url = tx_url + tx_addr
        print(url)
        resp = ping(url)
        if (resp):
            bURL = 'https://explorer.binance.org/address/'
            urlLink = bURL + tx_addr
            arrayLinks.append(urlLink)

            txArray = resp['trade']
            print(len(txArray))
            arrayTrades.append(resp['total'])

            tradeArray = []
            qty = 0
            for y in range(0, len(txArray)):
                order = txArray[y]
                print(order)
                qty += float(order['quantity'])

            arrayQty.append(qty)
            print(qty)
        else:
            arrayLinks.append('urlLink')
            arrayQty.append(0)
            arrayTrades.append(0)

        print('addr: {} qty: {} trades: {} links: {}'.format(len(arrayAddresses), len(arrayQty),len(arrayTrades),len(arrayLinks)))
        # Now write the results
        if (arrayQty[x] > 0):
            traders = traders + 1
            values = [
                [arrayAddresses[x], str(arrayQty[x]), str(arrayLinks[x]), str(arrayTrades[x])
                ]
            ]
            body = {
                'values': values
            }

            result = service.spreadsheets().values().append(
                spreadsheetId=SAMPLE_SPREADSHEET_ID, range=SAMPLE_RANGE_NAME,
                valueInputOption=value_input_option, body=body).execute()
            print('{0} cells appended.'.format(result \
                .get('updates') \
                .get('updatedCells')))

    # Write Holders
    values = [
        [
        str(len(arrayAddresses))
        ],
        ]
    data = [
        {
        'range': 'Sheet1!D2',
        'values': values
        },
    ]
    body = {
        'valueInputOption': value_input_option,
        'data': data
    }
    result = service.spreadsheets().values().batchUpdate(
        spreadsheetId=SAMPLE_SPREADSHEET_ID, body=body).execute()

    # Write Traders
    values = [
        [
        str(traders)
        ],
        ]
    data = [
        {
        'range': 'Sheet1!E2',
        'values': values
        },
    ]
    body = {
        'valueInputOption': value_input_option,
        'data': data
    }
    result = service.spreadsheets().values().batchUpdate(
        spreadsheetId=SAMPLE_SPREADSHEET_ID, body=body).execute()


if __name__ == '__main__':
    main()
